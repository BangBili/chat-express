const makeValidation = require('@withvoid/make-validation')
const UserModel = require('../models/User')


const USER_TYPES =  {
    CONSUMER: "consumer",
    SUPPORT: "support",
  };

module.exports = {
    onGetAllUsers: async (req, res) => {

    },
    // onGetUserById: async (req, res) => { 
    //     UserModel.findOne({_id:req.params.id})
    //                 .then((data) => {
    //                     return res.status(200).json({success: true, user: data})
    //                 })
    //                 .catch((err) => {res.status(400).json({success:false, message: 'data tidak ketemu', error:err} )})
    // },

    onGetUserById: async (req, res) => {
        try{
            const user = await UserModel.getUserById(req.params.id)
            return res.status(200).json({success: true, user: user})
        } catch(error) {
            // throw error
            return res.status(500).json({success: false})
        }
    },

    onCreateUser: async (req, res) => {
        try {
            const validation = makeValidation(types => ({
                payload: req.body,
                checks: {
                    firstName: { type: types.string },
                    lastName: { type: types.string },
                    type: { type: types.enum, options: { enum: USER_TYPES } },
                }
            }))
            if (!validation.success) {
                return res.status(400).json(validation)
            }
            const {firstName, lastName, type} = req.body
            const user = await UserModel.createUser(firstName, lastName, type)
            return res.status(200).json({ success: true, user: user })
        } catch (err) {
            return res.status(500).json({ success: false, error: err })
            // throw err
        }
    },
    onDeleteUserById: async (req, res) => { 
        try {
            const result = await UserModel.deleteUser(req.params._id)
            return res.status(200).json({success: true, result: result})
        } catch (error) {
            return res.status(500).json({success: false})
        }
    },
}