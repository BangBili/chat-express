const mongoose = require('mongoose')

const CHAT_ROOM_TYPES = {
    CONSUMER_TO_CONSUMER: "consumer-to-consumer",
    CONSUMER_TO_SUPPORT: "consumer-to-support",
}

const chatRoomSchema = new mongoose.Schema({
    userIds: Array,
    type: String,
    chatInitiator: String,
},{
    timestamps: true,
    collection:'chatrooms'
})

chatRoomSchema.statics.initiateChat = async function(userId, type, chatInitiator) {
    try {
        const availableRoom = await this.findOne({
            userIds: {
                $size: userIds.length,
                $all: [...userIds],
              },
              type,
        })
        if(availableRoom) {
            return {
                isNew: false,
                message: 'retrieving an old chat room',
                chatRoomId: availableRoom._doc._id,
                type: availableRoom._doc.type,
            }
        }
        const newRoom = await this.create({userId, type, chatInitiator})
        return {
            isNew : true,
            message: 'creating a new chatroom',
            chatRoomId: newRoom._doc._id,
            type: newRoom._doc.type
        }
    } catch(err) {return res.status(200).json(error)}
}

module.exports = ChatRoom = mongoose.model('ChatRoom', chatRoomSchema)