const mongoose = require('mongoose')

const userSchema = new mongoose.Schema(
    {
        firstName: String,
        lastName: String,
        type:String
    },
    {
        timestamps: true,
        collection: "users"
    }
)
userSchema.statics.getUserById = async function(id) {
    try{
        const user = await this.findOne({_id:id})
        if(! user) throw ({error: 'no user with id found'})

        return user
    } catch(error) {
        throw error
    }
}
userSchema.statics.createUser = async function(firstName, lastName, type){
    try{
        const user = await this.create({firstName, lastName, type})
        return user
    } catch(error) {
        throw error
    }
}
userSchema.statics.deleteUser = async function(id) {
    try{
        const result =await this.deleteOne(id)
        return result
    } catch(error) {
        throw error
    }
}

module.exports = User = mongoose.model('User', userSchema)
