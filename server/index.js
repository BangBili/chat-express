const http = require('http')
const express = require('express')
const logger = require('morgan')
const cors = require('cors')

// routes
const indexRoute = require('./routes/index')
const userRoute = require('./routes/user')
const chatRoomRoute = require('./routes/chatRoom')
const deleteRoute = require('./routes/delete')

// mongoDb
require('./config/mongo')

// middleware
const {decode} = require('./middlewares/jwt')

// configure
const app = express()

// port
const port = process.env.PORT || "3000"
app.set("port", port)

app.use(logger("dev"))
app.use(express.json())
app.use(express.urlencoded({extended:false}))

app.use('/', indexRoute)
app.use('/users', userRoute)
app.use('/room',decode, chatRoomRoute)
app.use('/delete',decode, deleteRoute)

// catch 404 atau not found
app.use('*', (req, res) => {
    return res.status(404).json({
        success: false,
        message: 'API endpoint ga ketemu'
    })
})

// buat HTTP server
const server = http.createServer(app)
server.listen(port)
server.on("listening",() => {
    console.log("listening on port 3000")
})





