class WebSockets {
    users = [];
    connection(client) {
        // jika event hangus, maka chat room di nonaktifkan
        client.on('disconnect', () => {
            this.users = this.users.filter((user) => user.socketId !== client.id)
        })
        // menambahkan subscriber jika user benar
        client.on('subscribe',(room, otherUserId = "") => {
            this.subscribeOtherUser(room, otherUserId)
            client.join(room)
        })
        // keluar percakapan
        client.on('unsubscribe', (room) => {
            client.leave(room)
        })
    }
    subscribeOtherUser(room, otherUserId) {
        const userSockets = this.users.filter(
            (user) => user.userId === otherUserId
        )
        userSockets.map((userInfo) => {
            const socketConn = global.io.sockets.connected(userInfo.socketId)
            if(socketConn) {
                socketConn.join(room)
            }
        })
    }
}

module.exports = new WebSockets()