const jwt = require('jsonwebtoken')
const UserModel = require('../models/User')

const SECRET_KEY = 'some-secret-key'
// melakukan setting jwt
const encode = (req, res, next) => { 
    try {
        const {userId} =req.params
        const user = await UserModel.getUserById(userId)
        const payload = {
            userId: user._id,
            userType: user.type
        }
        const authToken = jwt.sign(payload, SECRET_KEY)
        console.log('Auth', authToken);
        next()
    } catch(err) {
        return res.status(400).json({success: false, message: err.error})
    }
}

// membongkar kode jwt
const decode = (req, res, next) => { 
    try{
        if(!req.headers['authorization']) {
            return res.status(400).json({ success:false, message:"no access token provided!"})
        }
        const accessToken = req.headers.authorization.split(' ')[1]
        try{
            const decoded = jwt.verify(accessToken, SECRET_KEY)
            req.userId = decoded.userId
            req.userType = decoded.type
            return next()
        } catch(err){
            return res.status(500).json({success: false, message: err.message})
        }
    } catch(err) {
        return res.status(500).json({success: false, message:err.message})
    }
}

module.exports = {
    decode, encode
}